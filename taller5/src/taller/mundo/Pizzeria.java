package taller.mundo;

import taller.estructuras.IHeap;
import taller.estructuras.MaxPQ;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO 
	private MaxPQ<Pedido> pedidosRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
	private Pedido[] recibidos;
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private IHeap<Pedido> pedidosPorDespachar;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	private Pedido[] porDespachar;
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos=null;
		porDespachar=null;
		// TODO 
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido a=new Pedido(nombreAutor,precio,cercania);
		pedidosRecibidos.add(a);
		// TODO 
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		return pedidosRecibidos.poll();
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
	    return  pedidosPorDespachar.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
    	 // TODO 
    	int i=0;
        while(pedidosRecibidos.isEmpty())
        {
        	recibidos[i]=pedidosRecibidos.poll();
        	pedidosPorDespachar.add(recibidos[i]);
        	i++;
        }
        return  recibidos;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 int i=0;
         while(pedidosRecibidos.isEmpty())
         {
         	porDespachar[i]=pedidosPorDespachar.poll();
         	pedidosPorDespachar.add(porDespachar[i]);
         	i++;
         }
         return  porDespachar;
         
     }
}
